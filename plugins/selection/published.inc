<?php
/**
 * @file
 * published.inc
 */

$plugin = array(
  'title' => t('Published by language (with optional filter by bundle)'),
  'class' => 'EntityReferencePublished_SelectionHandler_Published',
  'weight' => -100,
);
