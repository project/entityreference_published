<?php
/**
 * @file
 * Entity Reference Published include plugin file.
 */

$plugin = array(
  'title' => t('Simple by language (with optional filter by bundle)'),
  'class' => 'EntityReferencePublished_SelectionHandler_GenericByLanguage',
  'weight' => -100,
);
